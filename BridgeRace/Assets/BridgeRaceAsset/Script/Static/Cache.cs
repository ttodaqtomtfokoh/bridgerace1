using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cache 
{

    #region GetPathControl
    private static Dictionary<Collider, PathControl> pathControls = new Dictionary<Collider, PathControl>();

    public static PathControl GetPathControl(Collider collider)
    {
        if (!pathControls.ContainsKey(collider))
        {
            PathControl pathControl = collider.GetComponent<PathControl>();

            pathControls.Add(collider, pathControl);
        }

        return pathControls[collider];
    }
    #endregion

    #region GetCharacterControl
    private static Dictionary<GameObject, CharacterControl> characterControls = new Dictionary<GameObject, CharacterControl>();

    public static CharacterControl GetCharacterControl(GameObject obj)
    {
        if (!characterControls.ContainsKey(obj))
        {
            CharacterControl characterControl = obj.GetComponent<CharacterControl>();
            characterControls.Add(obj, characterControl);
        }

        return characterControls[obj];

    }
    #endregion

    #region GetBlockColor
    private static Dictionary<GameObject, BlockColor> blockColors = new Dictionary<GameObject, BlockColor>();

    public static BlockColor GetBlockColor(GameObject obj)
    {
        if (!blockColors.ContainsKey(obj))
        {
            BlockColor blockColor = obj.GetComponent<BlockColor>();
            blockColors.Add(obj, blockColor);
        }

        return blockColors[obj];

    }
    #endregion

    #region GetHitPowerup
    private static Dictionary<Collider, HitPowerup> hitPowerups = new Dictionary<Collider, HitPowerup>();

    public static HitPowerup GetHitPowerup(Collider collider)
    {
        if (!hitPowerups.ContainsKey(collider))
        {
            HitPowerup hitPowerup = collider.GetComponent<HitPowerup>();

            hitPowerups.Add(collider, hitPowerup);
        }

        return hitPowerups[collider];
    }
    #endregion

    #region GetBridgeControl
    private static Dictionary<Transform, BridgeControl> bridgeControls = new Dictionary<Transform, BridgeControl>();

    public static BridgeControl GetBridgeControl(Transform transform)
    {
        if (!bridgeControls.ContainsKey(transform))
        {
            BridgeControl bridgeControl = transform.GetComponent<BridgeControl>();

            bridgeControls.Add(transform, bridgeControl);
        }

        return bridgeControls[transform];
    }
    #endregion

    #region GetLevelControl
    private static Dictionary<GameObject, LevelControl> levelcontrols = new Dictionary<GameObject, LevelControl>();

    public static LevelControl GetLevelControl(GameObject obj)
    {
        if (!levelcontrols.ContainsKey(obj))
        {
            LevelControl levelControl = obj.GetComponent<LevelControl>();

            levelcontrols.Add(obj, levelControl);
        }

        return levelcontrols[obj];
    }
    #endregion

    #region GetFloorControl
    private static Dictionary<GameObject, FloorControl> floorcontrols = new Dictionary<GameObject, FloorControl>();

    public static FloorControl GetFloorControl(GameObject obj)
    {
        if (!floorcontrols.ContainsKey(obj))
        {
            FloorControl floorControl = obj.GetComponent<FloorControl>();

            floorcontrols.Add(obj, floorControl);
        }

        return floorcontrols[obj];
    }
    #endregion

    #region GetTransform
    private static Dictionary<GameObject, Transform> transformDict = new Dictionary<GameObject, Transform>();

    public static Transform GetTransform(GameObject obj)
    {
        if (!transformDict.ContainsKey(obj))
        {
            Transform transform = obj.transform;

            transformDict.Add(obj, transform);
        }

        return transformDict[obj];
    }
    #endregion

    #region GetCollider
    private static Dictionary<GameObject, Collider> colliderDict = new Dictionary<GameObject, Collider>();

    public static Collider GetCollider(GameObject obj)
    {
        if (!colliderDict.ContainsKey(obj))
        {
            Collider collider = obj.GetComponent<Collider>();

            colliderDict.Add(obj, collider);
        }

        return colliderDict[obj];
    }
    #endregion

    #region GetRigidBody
    private static Dictionary<GameObject, Rigidbody> rigidDict = new Dictionary<GameObject, Rigidbody>();

    public static Rigidbody GetRigidbody(GameObject obj)
    {
        if (!rigidDict.ContainsKey(obj))
        {
            Rigidbody rigidbody = obj.GetComponent<Rigidbody>();

            rigidDict.Add(obj, rigidbody);
        }

        return rigidDict[obj];
    }
    #endregion

    #region WaitForSeconds
    public static WaitForSeconds waitFor(float time)
    {
        return new WaitForSeconds(time);
    }
    #endregion
}
