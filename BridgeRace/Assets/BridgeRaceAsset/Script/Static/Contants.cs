using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contants
{
    public static int LEVEL = 1;
    public static int MAX_LEVEL = 3;

    public static string NULL_STRING = "";

    public const int POOL_LEN = 30;
    public static string TAG_FLOOR = "Floor";
    public static Quaternion BRICK_ROTATION = Quaternion.Euler(0, 90, 0);

    public static string DEFAULT_PLAYER_NAME = "Player";
    public const int PLAYER_NUM = 4;
    public const int MAX_BRICK = 10;
    public const int PLAYER_LAYER = 3;

    public static string TAG_ANIM_VELOCITY = "velocity";
    public static string TAG_ANIM_ISFALL = "isFall";
    public static string TAG_ANIM_RESULT = "Result";

    public static string TAG_BRIDGE_PATH = "BridgePath";
    public const int MAX_BRICK_PLACED = 8;
    public const int BRIDGE_LAYER = 11;
    public static Vector3 BRICK_POS = new Vector3(0, 0.5f, 1);
    public static Vector3 BLOCK_POS = new Vector3(0, 0.6f, 1);
    public static Vector3 PLACE_BRICK = new Vector3(0, -1.6f, 1.25f);
    public static Vector3 PLACE_BLOCK= new Vector3(0, -1, 0.6f);

    public const float TIME_SHIELD = 3;
    public const float TIME_SPEEDUP = 5;
    public const float SPEEDUP = 1.25f;
    public static float[] SPAWN_TIME = { 5, 10 };
    public const float SPAWN_RANGE = 3.5f;

    public static int[] BRICKSPAWN_SIZE = { 4, 7 };

    public const int COUNTDOWN_TIME = 3;

    public static Vector3 FLOOR_POS = new Vector3(0, 3.6f, 18f);

    public static Vector3 CAM_END = new Vector3(0, 5, -5);
}
