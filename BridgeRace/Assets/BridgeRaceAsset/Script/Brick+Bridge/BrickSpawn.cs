using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickSpawn : MonoBehaviour
{
    private int row, col;
    private BrickPooler pool;
    [HideInInspector] public int floor;
    public List<GameObject>[] brick_list = { new List<GameObject>(), new List<GameObject>(), new List<GameObject>(), new List<GameObject>() };

    void Start()
    {
        row = Contants.BRICKSPAWN_SIZE[0];
        col = Contants.BRICKSPAWN_SIZE[1];

        pool = GetComponent<BrickPooler>();
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                pool.SpawnFromPool(GetRandomColor(), GetPostition(i,j));
            }
        }
    }

    private Vector3 GetPostition(int row, int col) {
        return new Vector3(col-3, 0.075f, row-3) + Contants.FLOOR_POS * (floor - 1);
    }

    public ColorHandler.Color GetRandomColor() {
        var a = Random.Range(0, 4);
        if (a == 0)
        {
            return ColorHandler.Color.Red;
        }
        else if (a == 1)
        {
            return ColorHandler.Color.Blue;
        }
        else if (a == 2)
        {
            return ColorHandler.Color.Green;
        }
        else return ColorHandler.Color.Yellow;
    }



    public GameObject RemoveFromPool(ColorHandler.Color tag, GameObject obj)
    {
        Vector3 pos = obj.transform.position;
        pool.poolDictionary[tag].Enqueue(obj);
        obj.SetActive(false);
        StartCoroutine(SpawnFromRemoved(pos, 3));
        return obj;
    }

    IEnumerator SpawnFromRemoved(Vector3 position, float delay)
    {
        yield return Cache.waitFor(delay);
        var tag = GetRandomColor();
        pool.SpawnFromPool(tag,position);
    }

}
