using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathControl : MonoBehaviour
{
    public bool[] pending_reset;

    public void SetPendingReset(ColorHandler.Color color, bool status)
    {
        pending_reset[(int)color] = status;
    }
}
