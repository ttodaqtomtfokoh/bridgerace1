using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    public GameObject Play;
    public GameObject Bar;
    public Image Clip;
    private float fillProgress = 0f;
    public InputField playername;

    public void LoadGame() {
        Play.SetActive(false);
        Bar.SetActive(true);
        if (playername.text.CompareTo(Contants.NULL_STRING) != 0)
        {
            Contants.DEFAULT_PLAYER_NAME = playername.text;
        }
        StartCoroutine(LoadingScene(1));
    }

    IEnumerator LoadingScene(int SceneIndex)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneIndex);
        while (!asyncLoad.isDone)
        {
            fillProgress = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            Clip.fillAmount = fillProgress;
            yield return null;
        }
    }

    public void Playagain()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        SceneManager.LoadScene(0);
    }

    public void NextLevel()
    {
        Contants.LEVEL += 1;
        SceneManager.LoadScene(1);
    }
}
