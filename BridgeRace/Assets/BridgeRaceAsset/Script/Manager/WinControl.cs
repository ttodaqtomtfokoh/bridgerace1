using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinControl : MonoBehaviour
{
    public GameManager gamemanager;

    private void OnTriggerEnter(Collider collision)
    {
        CharacterControl winner = Cache.GetCharacterControl(collision.gameObject);
        Cache.GetCollider(gameObject).enabled = false;
        if (winner == gamemanager.player)
        {
            gamemanager.Win();
            return;
        }
        else
        {
            for (int i = 0; i < gamemanager.bot_num; ++i)
            {
                if (winner == gamemanager.bots[i])
                {
                    gamemanager.Lose(i);
                    return;
                }
            }
        }
    }
}
