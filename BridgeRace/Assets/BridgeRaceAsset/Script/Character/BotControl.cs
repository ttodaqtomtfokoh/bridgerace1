using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotControl : CharacterControl
{
    [Header("Bot")]
    private UnityEngine.AI.NavMeshAgent navMeshAgent;
    private bool path_reseted = false;
    private bool canrun = false;
    private Vector3 new_des;
    private bool Finish = false;
    private List<GameObject> BrickToFind;
    private int RandomBrick;
    private int last_bridge = -1;
    private int bridge_num;
    private float end_point;
    private int floor_num;
    private Transform brickpool;
    private BridgeControl[] bridges = { null, null, null, null };


    [HideInInspector] public bool started = false;
    [HideInInspector] public FloorControl[] floor;
    [HideInInspector] public Transform win;


    public override void Start()
    {
        base.Start();
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        BrickToFind = new List<GameObject>();
        
    }

    private void FloorSetting(int floor_pos)
    {
        floor_num = floor.Length;
        bridge_num = floor[floor_pos - 1].bridge_num;
        brickspawn = floor[floor_pos - 1].brickspawn;
        brickpool = Cache.GetTransform(brickspawn.gameObject);
        for (int i = 0; i < bridge_num; ++i)
        {
            bridges[i] = floor[floor_pos - 1].bridges[i];
        }
        end_point = floor[floor_pos - 1].up_floor;
        AddBrick();
    }

    private void UpFloor()
    {
        Debug.Log(at_floor);
        if (at_floor >= floor_num)
        {
            Finish = true;
            end_point = Mathf.Infinity;
        }
        else
        {
            at_floor += 1;
            FloorSetting(at_floor);
            Reset();
        }
    }

    private void StartFinding()
    {
        FloorSetting(at_floor);
        RandomBrick = GetRandom(5, 10);
        StartCoroutine(FindBrick(0.25f));
        navMeshAgent.speed = speed;
    }

    protected override void FallAnim()
    {
        base.FallAnim();
    }

    protected override void CanMove()
    {
        base.CanMove();
        Reset();
    }

    private int GetRandom(int low, int high)
    {
        return Random.Range(low, high);
    }


    public void UpdateMesh()
    {
        UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
    }

    private IEnumerator FindBrick(float delay)
    {
        bool quit = false;
        yield return Cache.waitFor(delay);
        
        for (int i = 0; i < BrickToFind.Count; ++i) {
            GameObject brick = BrickToFind[i];
            var pos = Cache.GetTransform(brick).position;
            if (brick.activeInHierarchy)
            {
                new_des = pos;
                navMeshAgent.SetDestination(new_des);
                quit = true;
                break;
            }
        }
        if (!quit)
        {
            if (brick > 0)
            {
                BuildBridge(FindBridge());
            }
            else
            {
                new_des = brickpool.position + new Vector3(GetRandom(0, 5), 0, GetRandom(0, 5));
                navMeshAgent.SetDestination(new_des);
            }
        }
    }

    private bool BuildLastBridge()
    {
        if (last_bridge != -1)
        {
            int block = bridges[last_bridge].blockpos[(int)color];
            int brick_num = bridges[last_bridge].brick_placed;
            if (block < brick_num) return false;
            else return true;
        }
        else return false;
    }

    private int FindBridge()
    {
        path_reseted = false;
        if (BuildLastBridge())
        {
            return last_bridge;
        }
        else
        {
            int min_bridge = -1;
            int min_brick = 10;
            for (int i = 0; i < bridge_num; ++i)
            {
                if (bridges[i].brick_placed < min_brick)
                {
                    min_bridge = i;
                    min_brick = bridges[i].brick_placed;
                }
            }
            last_bridge = min_bridge;
            Debug.Log("build new bridge " + min_bridge);
            return min_bridge;
        }
    }

    private void BuildBridge(int bridge)
    {
        RandomBrick = GetRandom(1, 10);
        new_des = Cache.GetTransform(bridges[bridge].block_end).position;
        navMeshAgent.SetDestination(new_des);
    }

    private void RunToWin()
    {
        new_des = win.position;
        navMeshAgent.SetDestination(new_des);
    }

    private void AddBrick()
    {
        int j = (int)color;
        int pool_len = Contants.POOL_LEN;
        for (int i = j * pool_len; i < j * pool_len + pool_len; ++i)
        {
            BrickToFind = brickspawn.brick_list[(int)color];
        }
    }

    public void CheckDonePath()
    {
        if (Finish && result == 0)
        {
            RunToWin();
        }
        else if (!navMeshAgent.pathPending)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    if (brick < RandomBrick)
                    {
                        StartCoroutine(FindBrick(0));
                    }
                    else
                    {
                        BuildBridge(FindBridge());
                    }
                }
            }
        }
    }

    private void Reset()
    {
        navMeshAgent.isStopped = true;
        navMeshAgent.ResetPath();
        path_reseted = true;
    }

    public override void Update()
    {
        base.Update();
        if (started)
        {
            StartFinding();
            started = false;
            canrun = true;
        }
        else if (!canMove)
        {
            Reset();
        }
        else if (canrun)
        {
                if (result != 0)
                {
                    if (!path_reseted) Reset();
                }
                else
                {
                    if (navMeshAgent.speed != speed) navMeshAgent.speed = speed;
                    velocity = navMeshAgent.velocity.magnitude;
                    if (transform.position.z >= end_point) UpFloor();
                    if (!Finish && brick == 0 && !path_reseted)
                    {
                        Reset();
                    }
                    CheckDonePath();
                }
        }
        
    }
}
