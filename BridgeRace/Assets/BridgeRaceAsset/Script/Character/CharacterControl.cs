using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    [Header("CharacterInput")]
    public ColorHandler.Color color;
    public Animator anim;
    public float speed;
    public GameObject shield_display;
    public GameObject speed_display;

    private int brick_layer;
    private int max_brick;
    private PathControl lastpath;
    private bool PlayEndAnim = false;
    private float rotation_y;
    private Transform back_transform;

    [SerializeField] private GameObject Back;
    [SerializeField] private GameObject character_name;

    [HideInInspector] public BrickSpawn brickspawn;
    [HideInInspector] public bool on_bridge = false;
    [HideInInspector] public bool shielded = false;
    [HideInInspector] public bool speeded = false;
    [HideInInspector] public bool canMove = true;
    [HideInInspector] public int result; //0: playing, 1: lose, 2:win
    [HideInInspector] public int brick;
    [HideInInspector] public float velocity;
    [HideInInspector] public Collider collider;
    [HideInInspector] public Transform transform;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public int at_floor = 1;



    public virtual void Start()
    {
        GetBrickLayer(color);
        transform = Cache.GetTransform(gameObject);
        collider = Cache.GetCollider(gameObject);
        rb = Cache.GetRigidbody(gameObject);
        back_transform = Cache.GetTransform(Back);
        brick = 0;
        shielded = false;
        lastpath = null;
        result = 0;
        max_brick = Contants.MAX_BRICK;
    }

    private void DisplayBack()
    {
        if (brick == 0)
        {
            Back.SetActive(false);
        }
        else
        {
            Back.SetActive(true);
            Vector3 new_Scale = back_transform.localScale;
            new_Scale.Set(1, brick, 1);
            back_transform.localScale = new_Scale;
        }
    }

    private void GetBrickLayer(ColorHandler.Color color)
    {
        brick_layer = (int)color + 6;
    }

    public void OnTriggerEnter(Collider collision)
    {
        int layer = collision.gameObject.layer;
        if (layer == brick_layer)
        {
            if (brick < max_brick)
            {
                brickspawn.RemoveFromPool(color, collision.gameObject);
                brick += 1;
                DisplayBack();
            }
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;
        BlockColor other_block = Cache.GetBlockColor(obj);
        Collider other_collider = Cache.GetCollider(obj);
        int layer = obj.layer;

        if (layer == Contants.PLAYER_LAYER)
        {
            var other = Cache.GetCharacterControl(obj);
            var other_brick = other.brick;
            if (on_bridge || other.on_bridge || !canMove || !other.canMove || brick == other_brick)
            {
                Debug.Log("collapse with no effect");
            }
            else if (brick < other_brick && canMove && !shielded)
            {
                FallAnim();
            }
        }
        else if (layer == Contants.BRIDGE_LAYER && brick > 0)
        {
            if (other_block.blockcolor == color)
            {
                other_block.bridge.PlaceBrick(color);
                brick -= 1;
                DisplayBack();
            }
            else Physics.IgnoreCollision(collider, other_collider);
        }
        else if (obj.CompareTag(Contants.TAG_FLOOR))
        {
            FloorControl floor_standing = Cache.GetFloorControl(obj);
            at_floor = floor_standing.floor;
            brickspawn = floor_standing.brickspawn;
        }
    }

    protected virtual void FallAnim()
    {
        anim.SetBool(Contants.TAG_ANIM_ISFALL, true);
        StartCoroutine(StandUp());
        canMove = false;
        brick = 0;
        DisplayBack();
    }

    private IEnumerator StandUp()
    {
        yield return Cache.waitFor(1f);
        anim.SetBool(Contants.TAG_ANIM_ISFALL, false);
        StartCoroutine(WaitCanMove());
    }

    private IEnumerator WaitCanMove()
    {
        yield return Cache.waitFor(0.5f);
        CanMove();
    }

    protected virtual void CanMove()
    {
        canMove = true;
    }


    public void OnBridge()
    {
        if (!on_bridge)
        {
            on_bridge = true;
            lastpath.SetPendingReset(color, false);
        }
    }

    public void OffBridge()
    {
        if (on_bridge)
        {
            on_bridge = false;
            if (lastpath != null) lastpath.SetPendingReset(color, true);
        }
    }

    public virtual void Update()
    {
        rotation_y = transform.rotation.y;
        character_name.transform.rotation = Quaternion.Euler(0, -rotation_y, 0);

        if (result == 0)
        {
            anim.SetFloat(Contants.TAG_ANIM_VELOCITY, velocity);
            RaycastHit hit;
            Ray OnBridgeRay = new Ray(transform.position, Vector3.down);
            //Debug.DrawRay(transform.position, Vector3.down * 50, Color.black);
            if (Physics.Raycast(OnBridgeRay, out hit, 50f))
            {
                if (hit.collider.CompareTag(Contants.TAG_BRIDGE_PATH))
                {
                    PathControl path = Cache.GetPathControl(hit.collider);
                    if (path != lastpath) lastpath = path;
                    OnBridge();
                    return;
                }
                else
                {

                    OffBridge();
                    return;
                }
            }
        }
        else if (!PlayEndAnim)
        {
            PlayEndAnim = true;
            brick = 0;
            DisplayBack();
            anim.SetInteger(Contants.TAG_ANIM_RESULT, result);
        }
    }
}
