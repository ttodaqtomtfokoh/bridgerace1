using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playername : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMesh playername;

    void Start()
    {
        playername.text = Contants.DEFAULT_PLAYER_NAME;
    }
}
